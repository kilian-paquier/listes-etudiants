package com.view;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

public class Notification extends JDialog {
    private JPanel globalPanel;
    private JLabel text;
    private JFrame parent;

    public Notification(JFrame parent, String text) {
        setContentPane(globalPanel);
        setText(text);
        setUndecorated(true);
        this.parent = parent;
        open();
    }

    public JLabel getText() {
        return text;
    }

    public void setText(String text) {
        this.text.setText(text);
    }

    public void open() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setSize(text.getWidth() + 20, 0);
        setVisible(true);
        setFocusable(false);

        new Thread(() -> {
            try {
                while (getHeight() < 75) {
                    setLocation(parent.getLocation().x + 12, parent.getLocation().y + parent.getHeight() - getHeight() - 17);
                    setSize(getWidth(), getHeight() + 5);
                    Thread.sleep(7);
                }

                Thread.sleep(2000);

                while (getHeight() > 0) {
                    setLocation(parent.getLocation().x + 12, parent.getLocation().y + parent.getHeight() - getHeight() - 17);
                    setSize(getWidth(), getHeight() - 5);
                    Thread.sleep(7);
                }
                dispose();
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
            }
        }).start();
    }

    @Override
    public JFrame getParent() {
        return parent;
    }

    public void setParent(JFrame parent) {
        this.parent = parent;
    }

}

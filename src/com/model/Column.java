package com.model;

import java.util.ArrayList;
import java.util.List;

public class Column {
    private String columnName; // Le nom de la colonne
    private int columnNumber; // Le numéro de la colonne
    private List<String> values; // Les différentes valeurs que prend la colonne

    /**
     * Constructeur de la colonne prennant différents paramètres
     *
     * @param columnName   Le nom de la colonne
     * @param columnNumber Le numéro de la colonne
     * @param values       Les valeurs de la colonne
     */
    public Column(String columnName, int columnNumber, List<String> values) {
        this.columnName = columnName;
        this.columnNumber = columnNumber;
        if (values != null)
            this.values = values;
        else
            this.values = new ArrayList<>();
    }

    /**
     * Fonction permettant d'ajouter une valeur à la colonne si elle n'existe pas déjà
     *
     * @param value La valeur à ajouter
     * @return <code>true</code> si la valeur est ajoutée, <code>false</code> sinon
     */
    boolean addValue(String value) {
        if (!valueExists(value)) {
            values.add(value);
            return true;
        }
        return false;
    }

    /**
     * Fonction retournant le nom de la colonne
     *
     * @return Le nom de la colonne
     */
    public String getColumnName() {
        return columnName;
    }

    /**
     * Fonction retournant les listes des valeurs prises par la colonne
     *
     * @return Les valeurs sous forme d'une liste
     */
    public List<String> getValues() {
        return values;
    }

    /**
     * Fonction changeant intégralement la liste des valeurs prises par la colonne
     *
     * @param values La nouvelle liste des valeurs
     */
    public void setValues(List<String> values) {
        this.values = values;
    }

    /**
     * Fonction retournant un booléen en fonction de si la valeur passée en paramètre existe dans la liste des valeurs
     *
     * @param value La valeur à vérifier
     * @return Le booléen si la valeur exxiste ou non
     */
    private boolean valueExists(String value) {
        return values.contains(value);
    }

    /**
     * Fonction retournant le numéro de la colonne
     *
     * @return Le numéro de la colonne
     */
    public int getColumnNumber() {
        return columnNumber;
    }
}

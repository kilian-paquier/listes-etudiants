package com.controler;

import com.model.Degree;
import com.model.Polytech;
import com.view.MainView;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

abstract class PropertiesControler {
    /**
     * Fonction définissant les actions d'enregistrement des paramètres DI3
     *
     * @param controler Le controleur contenant la vue, le modèle, et les paramètres
     */
    static void saveSettingsDI3(AppControler controler) {
        MainView mainView = controler.getMainView();
        Polytech polytech = controler.getPolytech();
        Properties config = controler.getConfig();

        mainView.getComboBoxColonneRedoublantDI3().addItemListener(e -> {
            mainView.getComboBoxValeurRedoublantDI3().removeAllItems();
            for (String val : polytech.getFileAnalysis(Degree.Third).getColumn(String.valueOf(mainView.getComboBoxColonneRedoublantDI3().getSelectedItem())).getValues()) {
                mainView.getComboBoxValeurRedoublantDI3().addItem(val);
            }
            config.setProperty("redoubleColDI3", String.valueOf(mainView.getComboBoxColonneRedoublantDI3().getSelectedItem()));
            config.setProperty("redoubleValDI3", String.valueOf(mainView.getComboBoxValeurRedoublantDI3().getSelectedItem()));
            saveToXML(config);
        });

        mainView.getComboBoxColonneMundus().addItemListener(e -> {
            mainView.getComboBoxValeurMundus().removeAllItems();
            for (String val : polytech.getFileAnalysis(Degree.Third).getColumn(String.valueOf(mainView.getComboBoxColonneMundus().getSelectedItem())).getValues()) {
                mainView.getComboBoxValeurMundus().addItem(val);
            }
            config.setProperty("mundusCol", String.valueOf(mainView.getComboBoxColonneMundus().getSelectedItem()));
            config.setProperty("mundusVal", String.valueOf(mainView.getComboBoxValeurMundus().getSelectedItem()));
            saveToXML(config);
        });

        mainView.getComboBoxValeurRedoublantDI3().addItemListener(e -> {
            config.setProperty("redoubleValDI3", String.valueOf(mainView.getComboBoxValeurRedoublantDI3().getSelectedItem()));
            saveToXML(config);
        });

        mainView.getComboBoxValeurMundus().addItemListener(e -> {
            config.setProperty("mundusVal", String.valueOf(mainView.getComboBoxValeurMundus().getSelectedItem()));
            saveToXML(config);
        });
    }

    /**
     * Fonction définissant les actions d'enregistrement des paramètres DI4
     * @param controler Le controleur contenant la vue, le modèle, et les paramètres
     */
    static void saveSettingsDI4(AppControler controler) {
        MainView mainView = controler.getMainView();
        Polytech polytech = controler.getPolytech();
        Properties config = controler.getConfig();

        mainView.getComboBoxColonneRedoublantDI4().addItemListener(e -> {
            mainView.getComboBoxValeurRedoublantDI4().removeAllItems();
            for (String val : polytech.getFileAnalysis(Degree.Fourth).getColumn(String.valueOf(mainView.getComboBoxColonneRedoublantDI4().getSelectedItem())).getValues()) {
                mainView.getComboBoxValeurRedoublantDI4().addItem(val);
            }
            config.setProperty("redoubleColDI4", String.valueOf(mainView.getComboBoxValeurRedoublantDI3().getSelectedItem()));
            config.setProperty("redoubleValDI4", String.valueOf(mainView.getComboBoxValeurRedoublantDI4().getSelectedItem()));
            saveToXML(config);
        });

        mainView.getComboBoxColonneMobiliteDI4().addItemListener(e -> {
            mainView.getComboBoxValeurMobiliteDI4().removeAllItems();
            for (String val : polytech.getFileAnalysis(Degree.Fourth).getColumn(String.valueOf(mainView.getComboBoxColonneMobiliteDI4().getSelectedItem())).getValues()) {
                mainView.getComboBoxValeurMobiliteDI4().addItem(val);
            }
            config.setProperty("mobiliteColDI4", String.valueOf(mainView.getComboBoxValeurMobiliteDI4().getSelectedItem()));
            config.setProperty("mobiliteValDI4", String.valueOf(mainView.getComboBoxValeurMobiliteDI4().getSelectedItem()));
            saveToXML(config);
        });

        mainView.getComboBoxValeurRedoublantDI4().addItemListener(e -> {
            config.setProperty("redoubleValDI4", String.valueOf(mainView.getComboBoxValeurRedoublantDI4().getSelectedItem()));
            saveToXML(config);
        });

        mainView.getComboBoxValeurMobiliteDI4().addItemListener(e -> {
            config.setProperty("mobiliteValDI4", String.valueOf(mainView.getComboBoxValeurMobiliteDI4().getSelectedItem()));
            saveToXML(config);
        });
    }

    /**
     * Fonction définissant les actions d'enregistrement des paramètres DI5
     * @param controler Le controleur contenant la vue, le modèle, et les paramètres
     */
    static void saveSettingsDI5(AppControler controler) {
        MainView mainView = controler.getMainView();
        Polytech polytech = controler.getPolytech();
        Properties config = controler.getConfig();

        mainView.getComboBoxColonneRedoublantDI5().addItemListener(e -> {
            mainView.getComboBoxValeurRedoublantDI5().removeAllItems();
            for (String val : polytech.getFileAnalysis(Degree.Fifth).getColumn(String.valueOf(mainView.getComboBoxColonneRedoublantDI5().getSelectedItem())).getValues()) {
                mainView.getComboBoxValeurRedoublantDI5().addItem(val);
            }
            config.setProperty("redoubleColDI5", String.valueOf(mainView.getComboBoxValeurRedoublantDI5().getSelectedItem()));
            config.setProperty("redoubleValDI5", String.valueOf(mainView.getComboBoxValeurRedoublantDI5().getSelectedItem()));
            config.setProperty("redoubleColDI5", String.valueOf(mainView.getComboBoxColonneRedoublantDI5().getSelectedItem()));
            config.setProperty("redoubleValDI5", String.valueOf(mainView.getComboBoxValeurRedoublantDI5().getSelectedItem()));
            saveToXML(config);
        });

        mainView.getComboBoxColonneMobiliteDI5().addItemListener(e -> {
            mainView.getComboBoxValeurMobiliteDI5().removeAllItems();
            for (String val : polytech.getFileAnalysis(Degree.Fifth).getColumn(String.valueOf(mainView.getComboBoxColonneMobiliteDI5().getSelectedItem())).getValues()) {
                mainView.getComboBoxValeurMobiliteDI5().addItem(val);
            }
            config.setProperty("mobiliteColDI5", String.valueOf(mainView.getComboBoxColonneMobiliteDI5().getSelectedItem()));
            config.setProperty("mobiliteValDI5", String.valueOf(mainView.getComboBoxValeurMobiliteDI5().getSelectedItem()));
            saveToXML(config);
        });

        mainView.getComboBoxValeurRedoublantDI5().addItemListener(e -> {
            config.setProperty("redoubleValDI5", String.valueOf(mainView.getComboBoxValeurRedoublantDI5().getSelectedItem()));
            saveToXML(config);
        });

        mainView.getComboBoxValeurMobiliteDI5().addItemListener(e -> {
            config.setProperty("mobiliteValDI5", String.valueOf(mainView.getComboBoxValeurMobiliteDI5().getSelectedItem()));
            saveToXML(config);
        });
    }

    /**
     * Méthode exportant dans le fichier xml les paramètres
     *
     * @param config La configuration à exporter
     */
    static void saveToXML(Properties config) {
        try {
            config.storeToXML(new FileOutputStream(".\\resources\\parameters\\parameters.xml"), "Sauvegardé");
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}

package com.controler;

import com.model.*;
import com.view.MainView;
import com.view.Notification;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

abstract class TemplateControler {
    /**
     * Méthode définissant les actions pour la liste des templates DI3
     *
     * @param controler Le controleur
     */
    static void actionTemplatesDI3(AppControler controler) {
        MainView mainView = controler.getMainView();
        Polytech polytech = controler.getPolytech();

        mainView.getListTemplateDI3().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                String item = mainView.getListTemplateDI3().getSelectedValue();
                Template template = polytech.getFileAnalysis(Degree.Third).getTemplate(item);
                if (template != null) {
                    actionUpdateTable(template.getModel(), Degree.Third, controler); // MouseListener pour charger le tableau du template sélectionné
                    if (!template.getTemplateName().equals("Template vide")) {
                        mainView.getFieldDI3().setText(template.getTemplateName());
                        changeTextField(Degree.Third, controler, template.getTemplateName());
                    } else {
                        mainView.getFieldDI3().setText("");
                        changeTextField(Degree.Third, controler, "");
                    }
                }
            }
        });

        mainView.getModifierTemplateDI3().addActionListener(e -> {
            try {
                actionModifierTemplate(Degree.Third, controler); // Fonction de modification du template sélectionné
            } catch (IOException e1) {
                new Notification(mainView, e1.getMessage());
            }
        });

        mainView.getAjouterTemplateDI3().addActionListener(e -> {
            try {
                actionAjouterTemplate(Degree.Third, controler);
            } catch (IOException e1) {
                new Notification(mainView, e1.getMessage());
            }
        });

        mainView.getSupprimerTemplateDI3().addActionListener(e -> {
            try {
                // Action de suppression d'un template
                actionSupprimerTemplate(Degree.Third, mainView.getListTemplateDI3().getSelectedValue(), controler);
                mainView.getFieldDI3().setText("");
            } catch (IOException e1) {
                new Notification(mainView, e1.getMessage());
            }
        });
    }

    /**
     * Méthode définissant les actions pour la liste des templates DI4
     *
     * @param controler Le controleur
     */
    static void actionTemplatesDI4(AppControler controler) {
        MainView mainView = controler.getMainView();
        Polytech polytech = controler.getPolytech();

        mainView.getListTemplateDI4().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                String item = mainView.getListTemplateDI4().getSelectedValue();
                Template template = polytech.getFileAnalysis(Degree.Fourth).getTemplate(item);
                if (template != null) {
                    actionUpdateTable(template.getModel(), Degree.Fourth, controler); // Action de chargement d'un template
                    if (!template.getTemplateName().equals("Template vide")) {
                        mainView.getFieldDI4().setText(template.getTemplateName());
                        changeTextField(Degree.Fourth, controler, template.getTemplateName());
                    } else {
                        mainView.getFieldDI4().setText("");
                        changeTextField(Degree.Fourth, controler, "");
                    }
                }
            }
        });

        mainView.getAjouterTemplateDI4().addActionListener(e -> {
            try {
                actionAjouterTemplate(Degree.Fourth, controler);
            } catch (IOException e1) {
                new Notification(mainView, e1.getMessage());
            }
        });

        mainView.getModifierTemplateDI4().addActionListener(e -> {
            try {
                actionModifierTemplate(Degree.Fourth, controler); // Action de modification d'un template
            } catch (IOException e1) {
                new Notification(mainView, e1.getMessage());
            }
        });

        mainView.getSupprimerTemplateDI4().addActionListener(e -> {
            try {
                // Action de suppression d'un template
                actionSupprimerTemplate(Degree.Fourth, mainView.getListTemplateDI4().getSelectedValue(), controler);
                mainView.getFieldDI4().setText("");
            } catch (IOException e1) {
                new Notification(mainView, e1.getMessage());
            }
        });
    }

    /**
     * Méthode définissant les actions pour la liste des templates DI5
     *
     * @param controler Le controleur
     */
    static void actionTemplatesDI5(AppControler controler) {
        MainView mainView = controler.getMainView();
        Polytech polytech = controler.getPolytech();

        mainView.getListTemplateDI5().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                String item = mainView.getListTemplateDI5().getSelectedValue();
                Template template = polytech.getFileAnalysis(Degree.Fifth).getTemplate(item);
                if (template != null) {
                    actionUpdateTable(template.getModel(), Degree.Fifth, controler); // Action de chargement d'un template
                    if (!template.getTemplateName().equals("Template vide")) {
                        mainView.getFieldDI5().setText(template.getTemplateName());
                        changeTextField(Degree.Fifth, controler, template.getTemplateName());
                    } else {
                        mainView.getFieldDI5().setText("");
                        changeTextField(Degree.Fifth, controler, "");
                    }
                }
            }
        });

        mainView.getAjouterTemplateDI5().addActionListener(e -> {
            try {
                actionAjouterTemplate(Degree.Fifth, controler);
            } catch (IOException e1) {
                new Notification(mainView, e1.getMessage());
            }
        });

        mainView.getModifierTemplateDI5().addActionListener(e -> {
            try {
                actionModifierTemplate(Degree.Fifth, controler); // Action de modification d'un template
            } catch (IOException e1) {
                new Notification(mainView, e1.getMessage());
            }
        });

        mainView.getSupprimerTemplateDI5().addActionListener(e -> {
            try {
                // Action de suppression d'un template
                actionSupprimerTemplate(Degree.Fifth, mainView.getListTemplateDI5().getSelectedValue(), controler);
                mainView.getFieldDI5().setText("");
            } catch (IOException e1) {
                new Notification(mainView, e1.getMessage());
            }
        });
    }

    /**
     * Fonction d'ajout d'un template à la liste des templates
     *
     * @param degree Le degré de l'ajout
     * @param controler Le controleur
     * @throws IOException Une exception
     */
    private static void actionAjouterTemplate(Degree degree, AppControler controler) throws IOException {
        MainView mainView = controler.getMainView();
        Polytech polytech = controler.getPolytech();

        // OptionPane qui demande la saisi du nom du template
        Object templateObject = JOptionPane.showInputDialog(mainView, "Nom du template à créer", "Template", JOptionPane.PLAIN_MESSAGE);
        String templateName = String.valueOf(templateObject); // Le nom du template
        if (templateName.equals(""))
            throw new IOException("Nom de template \"" + templateName + "\" impossible");
        if (templateObject != null) { // On vérifie que l'utilisateur n'a pas annulé la saisie
            /*
            On regarde le degré en paramètre
             */
            if (degree.equals(Degree.Third)) {
                // On créé un template puis on lui ajoute le vecteur du modèle du tableau
                Template template = new Template(templateName, new ModelTable());
                template.getModel().setDataVector(((ModelTable) mainView.getTableDI3().getModel()).getDataVector(), ((ModelTable) mainView.getTableDI3().getModel()).getColumnNames());
                template.setTemplateDegree(degree);
                polytech.getFileAnalysis(degree).addTemplate(template);

                // On sérialise le template et on actualise la liste des templates
                Serialize.serializeTemplate(template, new File(".\\resources\\templates\\DI3\\" + template.getTemplateName() + ".ser"));
                setListModel(degree, controler);
                mainView.getListTemplateDI3().setSelectedIndex(mainView.getListTemplateDI3().getModel().getSize() - 1);
                mainView.getFieldDI3().setText(templateName);
            }

            /*
            On regarde le degré en paramètre
             */
            if (degree.equals(Degree.Fourth)) {
                // On créé un template puis on lui ajoute le vecteur du modèle du tableau
                Template template = new Template(templateName, new ModelTable());
                template.getModel().setDataVector(((ModelTable) mainView.getTableDI4().getModel()).getDataVector(), ((ModelTable) mainView.getTableDI4().getModel()).getColumnNames());
                template.setTemplateDegree(degree);
                polytech.getFileAnalysis(degree).addTemplate(template);

                // On sérialise le template et on actualise la liste des templates
                Serialize.serializeTemplate(template, new File(".\\resources\\templates\\DI4\\" + template.getTemplateName() + ".ser"));
                setListModel(degree, controler);
                mainView.getListTemplateDI4().setSelectedIndex(mainView.getListTemplateDI4().getModel().getSize() - 1);
                mainView.getFieldDI4().setText(templateName);
            }

            /*
            On regarde le degré en paramètre
             */
            if (degree.equals(Degree.Fifth)) {
                // On créé un template puis on lui ajoute le vecteur du modèle du tableau
                Template template = new Template(templateName, new ModelTable());
                template.getModel().setDataVector(((ModelTable) mainView.getTableDI5().getModel()).getDataVector(), ((ModelTable) mainView.getTableDI5().getModel()).getColumnNames());
                template.setTemplateDegree(degree);
                polytech.getFileAnalysis(degree).addTemplate(template);

                // On sérialise le template et on actualise la liste des templates
                Serialize.serializeTemplate(template, new File(".\\resources\\templates\\DI5\\" + template.getTemplateName() + ".ser"));
                setListModel(degree, controler);
                mainView.getListTemplateDI5().setSelectedIndex(mainView.getListTemplateDI5().getModel().getSize() - 1);
                mainView.getFieldDI5().setText(templateName);
            }

            changeTextField(degree, controler, templateName);
        }
    }

    /**
     * Fonction de l'action pour
     *
     * @param degree   Le degré
     * @param template Le nom du template à supprimer
     * @param controler Le controleur
     * @throws IOException Une exception
     */
    private static void actionSupprimerTemplate(Degree degree, String template, AppControler controler) throws IOException {
        MainView mainView = controler.getMainView();
        Polytech polytech = controler.getPolytech();
        File templateDirectory = controler.getTemplateDirectory();

        File temp = new File(String.valueOf(templateDirectory));
        if (template == null)
            throw new IOException("Aucun template sélectionné"); // On vérifie que le template n'est pas null
        if (template.equals("Template vide"))
            throw new IOException("Template vide ne peut être supprimé"); // On vérifie que le template n'est pas le template vide

        polytech.getFileAnalysis(degree).deleteTemplate(template); // On supprime le template de la liste des templates

        /*
        On crée un chemin de fichier temporaire correspondant au chemin du dossier pour le degré pour lequel on veut supprimer le template
         */
        if (degree.equals(Degree.Third))
            temp = new File(temp.toString() + "\\DI3");
        if (degree.equals(Degree.Fourth))
            temp = new File(temp.toString() + "\\DI4");
        if (degree.equals(Degree.Fifth))
            temp = new File(temp.toString() + "\\DI5");

        /*
        On parours les fichiers de sérialisation des templates et on supprime celui correspondant au template supprimé
         */
        for (File tempFile : Objects.requireNonNull(temp.listFiles())) {
            if (tempFile.getName().equals(template + ".ser")) {
                if (!tempFile.delete())
                    new Notification(mainView, "Le fichier du template supprimé n'existe pas");
                break;
            }
        }

        // On remet le tableau initial
        if (degree.equals(Degree.Third) && polytech.getFileAnalysis(degree) != null) {
            compareModels((ModelTable) mainView.getTableDI3().getModel(), polytech.getFileAnalysis(degree).getTemplate("Template vide").getModel());
            mainView.getTableDI3().setModel(mainView.getTableDI3().getModel());
            mainView.getListTemplateDI3().setSelectedIndex(0);
            mainView.getFieldDI3().setText("");
        }

        // On remet le tableau initial
        if (degree.equals(Degree.Fourth) && polytech.getFileAnalysis(degree) != null) {
            compareModels((ModelTable) mainView.getTableDI4().getModel(), polytech.getFileAnalysis(degree).getTemplate("Template vide").getModel());
            mainView.getTableDI4().setModel(mainView.getTableDI4().getModel());
            mainView.getListTemplateDI4().setSelectedIndex(0);
            mainView.getFieldDI4().setText("");
        }

        // On remet le tableau initial
        if (degree.equals(Degree.Fifth) && polytech.getFileAnalysis(degree) != null) {
            compareModels((ModelTable) mainView.getTableDI5().getModel(), polytech.getFileAnalysis(degree).getTemplate("Template vide").getModel());
            mainView.getTableDI5().setModel(mainView.getTableDI5().getModel());
            mainView.getListTemplateDI5().setSelectedIndex(0);
            mainView.getFieldDI5().setText("");
        }

        // On actualise le modèle de la liste
        setListModel(degree, controler);
        changeTextField(degree, controler, "");
        new Notification(mainView, "Template supprimé");
    }

    /**
     * Fonction d'action pour modifier un template
     *
     * @param degree Le degré
     * @param controler Le controleur
     * @throws IOException Une exception
     */
    private static void actionModifierTemplate(Degree degree, AppControler controler) throws IOException {
        MainView mainView = controler.getMainView();
        Polytech polytech = controler.getPolytech();
        File templateDirectory = controler.getTemplateDirectory();

        /*
        On regarde le degré passé en paramètre
         */
        if (degree.equals(Degree.Third)) {
            // On récupère le nom du template et on le recherche et le retourne
            String templateName = mainView.getListTemplateDI3().getSelectedValue();

            Template template = polytech.getFileAnalysis(degree).getTemplate(templateName);
            conditionsModificationtemplate(template);

            template.getModel().setDataVector(((ModelTable) mainView.getTableDI3().getModel()).getDataVector(), ((ModelTable) mainView.getTableDI3().getModel()).getColumnNames());
            Serialize.serializeTemplate(template, new File(templateDirectory + "\\DI3\\" + templateName + ".ser"));

            // On désélectionne les cellules pouvant avoir été sélectionnée précédemment
            mainView.getTableDI3().changeSelection(mainView.getTableDI3().getSelectedRow(), mainView.getTableDI3().getSelectedColumn(), false, false);
        }

        /*
        On regarde le degré passé en paramètre
         */
        if (degree.equals(Degree.Fourth)) {
            // On récupère le nom du template et on le recherche et le retourne
            String templateName = mainView.getListTemplateDI4().getSelectedValue();

            Template template = polytech.getFileAnalysis(degree).getTemplate(templateName);
            conditionsModificationtemplate(template);

            template.getModel().setDataVector(((ModelTable) mainView.getTableDI4().getModel()).getDataVector(), ((ModelTable) mainView.getTableDI4().getModel()).getColumnNames());
            Serialize.serializeTemplate(template, new File(templateDirectory + "\\DI4\\" + templateName + ".ser"));


            // On désélectionne les cellules pouvant avoir été sélectionnée précédemment
            mainView.getTableDI4().changeSelection(mainView.getTableDI4().getSelectedRow(), mainView.getTableDI4().getSelectedColumn(), true, false);
        }

        /*
        On regarde le degré passé en paramètre
         */
        if (degree.equals(Degree.Fifth)) {
            // On récupère le nom du template et on le recherche et le retourne
            String templateName = mainView.getListTemplateDI5().getSelectedValue();

            Template template = polytech.getFileAnalysis(degree).getTemplate(templateName);
            conditionsModificationtemplate(template);

            template.getModel().setDataVector(((ModelTable) mainView.getTableDI5().getModel()).getDataVector(), ((ModelTable) mainView.getTableDI5().getModel()).getColumnNames());
            Serialize.serializeTemplate(template, new File(templateDirectory + "\\DI5\\" + templateName + ".ser"));

            // On désélectionne les cellules pouvant avoir été sélectionnée précédemment
            mainView.getTableDI5().changeSelection(mainView.getTableDI5().getSelectedRow(), mainView.getTableDI5().getSelectedColumn(), false, false);
        }

        new Notification(mainView, "Template modifié");
    }

    /**
     * Fonction d'action de mise à jour du tableau en fonction d'un template
     *
     * @param newModel Le modèle du template à charger
     * @param degree   Le degré
     * @param controler Le controleur
     */
    private static void actionUpdateTable(ModelTable newModel, Degree degree, AppControler controler) {
        MainView mainView = controler.getMainView();

        /*
        On regarde le degré en paramètre
         */
        if (degree.equals(Degree.Third)) {
            ModelTable oldModel = (ModelTable) mainView.getTableDI3().getModel();
            compareModels(oldModel, newModel); // On compare les modèles et on actualise l'ancien
            mainView.getTableDI3().setModel(oldModel);
        }

        /*
        On regarde le degré en paramètre
         */
        if (degree.equals(Degree.Fourth)) {
            ModelTable oldModel = (ModelTable) mainView.getTableDI4().getModel();
            compareModels(oldModel, newModel); // On compare les modèles et on actualise l'ancien
            mainView.getTableDI4().setModel(oldModel);
        }

        /*
        On regarde le degré en paramètre
         */
        if (degree.equals(Degree.Fifth)) {
            ModelTable oldModel = (ModelTable) mainView.getTableDI5().getModel();
            compareModels(oldModel, newModel); // On compare les modèles et on actualise l'ancien
            mainView.getTableDI4().setModel(oldModel);
        }
    }

    /**
     * Fonction de comparaison et de modification des modèles utilisé pour la modification de templates
     * @param oldModel L'ancien modèle qui sera actualisé
     * @param newModel Le nouveau modèle
     */
    private static void compareModels(ModelTable oldModel, ModelTable newModel) {
        if (oldModel.getColumnCount() <= newModel.getColumnCount()) {
            // cas ou le nouveau modele est plus grand
            for (int rowNum = 0; rowNum < oldModel.getRowCount(); rowNum++) { // On regarde chaque ligne
                for (int colNum = 0; colNum < oldModel.getColumnCount(); colNum += 2) { // On regarde chaque colonne pair (cellules ayant les nom des valeurs)
                    String value = String.valueOf(newModel.getDataVector().get(rowNum).get(colNum)); // On recupère la valeur du nouveau modèle
                    boolean bValue = Boolean.valueOf(String.valueOf(newModel.getDataVector().get(rowNum).get(colNum + 1))); // On récupère son booléen correspondant
                    if (oldModel.getDataVector().get(rowNum).contains(value) && value != null) {
                        // Si la ligne contient la valeur alors on actualise le booléen
                        oldModel.setValueAt(bValue, rowNum, oldModel.getDataVector().get(rowNum).indexOf(value) + 1);
                    }
                }
            }
        }
        else {
            //cas ou le nouveau modele est plus petit
            for (int rowNum = 0; rowNum < newModel.getRowCount(); rowNum++) { // On regarde chaque ligne
                for (int colNum = 0; colNum < newModel.getColumnCount(); colNum += 2) { // On regarde chaque colonne pair (cellules ayant les nom des valeurs)
                    String value = String.valueOf(newModel.getDataVector().get(rowNum).get(colNum)); // On recupère la valeur du nouveau modèle
                    boolean bValue = Boolean.valueOf(String.valueOf(newModel.getDataVector().get(rowNum).get(colNum + 1))); // On récupère son booléen correspondant
                    if (oldModel.getDataVector().get(rowNum).contains(value) && value != null) {
                        // Si la ligne contient la valeur alors on actualise le booléen
                        oldModel.setValueAt(bValue, rowNum, oldModel.getDataVector().get(rowNum).indexOf(value) + 1);
                    }
                }
            }
        }
    }

    /**
     * Fonction qui set le modèle de la liste des templates
     * @param degree Le degré
     * @param controler Le controleur
     */
    static void setListModel(Degree degree, AppControler controler) {
        Polytech polytech = controler.getPolytech();
        MainView mainView = controler.getMainView();

        DefaultListModel<String> listModel = new DefaultListModel<>();
        FileAnalysis fileAnalysis = polytech.getFileAnalysis(degree);

        for (Template template : fileAnalysis.getTemplates()) {
            if (!listModel.contains(template.getTemplateName()))
                listModel.addElement(template.getTemplateName());
        }

        if (degree.equals(Degree.Third)) {
            mainView.getListTemplateDI3().setModel(listModel);
            mainView.getListTemplateDI3().setSelectedIndex(0);
        }
        if (degree.equals(Degree.Fourth)) {
            mainView.getListTemplateDI4().setModel(listModel);
            mainView.getListTemplateDI4().setSelectedIndex(0);
        }
        if (degree.equals(Degree.Fifth)) {
            mainView.getListTemplateDI5().setModel(listModel);
            mainView.getListTemplateDI5().setSelectedIndex(0);
        }
    }

    /**
     * Méthode vérifiant les conditions pour la modification d'un template
     *
     * @param template Le template à modifier
     * @throws IOException L'exception levée si une condition n'est pas satisfaite
     */
    private static void conditionsModificationtemplate(Template template) throws IOException {
        if (template == null)
            throw new IOException("Aucun template sélectionné"); // On vérifie qu'il y a bien un template de sélectionné
        if (template.getTemplateName().equals("Template vide"))
            throw new IOException("Template vide ne peut être modifié"); // On vérifie que le template n'est pas le template vide

        // On redéfini le modèle grâce au vecteur du nouveau modèle puis on sérialise à nouveau le template
        template.setTemplateModel(new ModelTable());
    }

    /**
     * Méthode désérialisant les templates pour un dégré
     *
     * @param degree    Le degré (année) dont on veut désérialiser les templates
     * @param controler Le controleur
     */
    static void deserializeTemplates(Degree degree, AppControler controler) {
        File templateDirectory = controler.getTemplateDirectory();
        Polytech polytech = controler.getPolytech();
        MainView mainView = controler.getMainView();

        for (File directory : Objects.requireNonNull(templateDirectory.listFiles())) {
            String directoryName = null;
            if (degree.equals(Degree.Third))
                directoryName = "DI3";
            if (degree.equals(Degree.Fourth))
                directoryName = "DI4";
            if (degree.equals(Degree.Fifth))
                directoryName = "DI5";

            if (directory.getName().contains(Objects.requireNonNull(directoryName))) {
                for (File template : Objects.requireNonNull(directory.listFiles())) {
                    Template temp = Serialize.deserializeTemplate(template);
                    if (temp != null) { // On vérifie que temp est bien différent de null
                        try {
                            polytech.getFileAnalysis(temp.getTemplateDegree()).addTemplate(temp);
                        } catch (IOException e) {
                            new Notification(mainView, e.getMessage());
                        }
                    }
                }
            }
        }
        setListModel(degree, controler);
    }

    /**
     * Méthode permettant de changer l'élément sélectionné dans la combobox des types d'exportation en fonction du template sélectionné
     *
     * @param degree       Le degré du panel
     * @param controler    Le controleur
     * @param templateName Le nom du template
     */
    private static void changeTextField(Degree degree, AppControler controler, String templateName) {
        MainView mainView = controler.getMainView();

        boolean boolEmargement = templateName.toUpperCase().contains("EMAR") || templateName.toUpperCase().contains("ÉMAR");
        boolean boolNotes = false;
        if (!boolEmargement)
            boolNotes = templateName.toUpperCase().contains("NOTES") || templateName.toUpperCase().contains("NOTE");

        if (degree.equals(Degree.Third)) {
            if (boolEmargement)
                mainView.getTypeBoxDI3().setSelectedItem(InitControler.getTextExportEmargment());
            else if (boolNotes)
                mainView.getTypeBoxDI3().setSelectedItem(InitControler.getTextExportNotes());
            else
                mainView.getTypeBoxDI3().setSelectedIndex(0);
        }

        if (degree.equals(Degree.Fourth)) {
            if (boolEmargement)
                mainView.getTypeBoxDI4().setSelectedItem(InitControler.getTextExportEmargment());
            else if (boolNotes)
                mainView.getTypeBoxDI4().setSelectedItem(InitControler.getTextExportNotes());
            else
                mainView.getTypeBoxDI4().setSelectedIndex(0);
        }

        if (degree.equals(Degree.Fifth)) {
            if (boolEmargement)
                mainView.getTypeBoxDI5().setSelectedItem(InitControler.getTextExportEmargment());
            else if (boolNotes)
                mainView.getTypeBoxDI5().setSelectedItem(InitControler.getTextExportNotes());
            else
                mainView.getTypeBoxDI3().setSelectedIndex(0);
        }
    }
}

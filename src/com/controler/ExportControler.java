package com.controler;

import com.model.*;
import com.view.MainView;
import com.view.Notification;

import java.io.IOException;
import java.util.Properties;

abstract class ExportControler {
    /**
     * Fonction définissant quel type d'exportation se fera pour la panel DI3
     *
     * @param controler Le controleur contenant la vue et la modèle, qui permet de récupérer la comboBox pour le type d'exportation et autres objets utiles
     */
    static void exportDI3(AppControler controler) {
        MainView mainView = controler.getMainView();
        Polytech polytech = controler.getPolytech();

        mainView.getExportButtonDI3().addActionListener(e -> {
            try {
                setCaracteristicsBeforeExportDI3(controler);

                if (String.valueOf(mainView.getTypeBoxDI3().getSelectedItem()).equals(InitControler.getTextExportList()))
                    Utils.exportListToXLS((ModelTable) mainView.getTableDI3().getModel(), polytech.getFileAnalysis(Degree.Third).getStudents(), polytech.getFileAnalysis(Degree.Third).getFile(), mainView.getFieldDI3().getText());
                else if (String.valueOf(mainView.getTypeBoxDI3().getSelectedItem()).equals(InitControler.getTextExportNotes()))
                    Utils.exportNotesToXLS((ModelTable) mainView.getTableDI3().getModel(), polytech.getFileAnalysis(Degree.Third).getStudents(), polytech.getFileAnalysis(Degree.Third).getFile(), mainView.getFieldDI3().getText());
                else if (String.valueOf(mainView.getTypeBoxDI3().getSelectedItem()).contains(InitControler.getTextExportEmargment()))
                    Utils.exportListEmargement((ModelTable) mainView.getTableDI3().getModel(), polytech.getFileAnalysis(Degree.Third).getStudents(), polytech.getFileAnalysis(Degree.Third).getFile(), mainView.getFieldDI3().getText());


                new Notification(mainView, "Exportation de la liste terminée");
            } catch (IOException e1) {
                new Notification(mainView, e1.getMessage());
            }
        });
    }

    /**
     * Fonction définissant quel type d'exportation se fera pour le panel DI4
     *
     * @param controler Le controleur contenant la vue et la modèle, qui permet de récupérer la comboBox pour le type d'exportation et autres objets utiles
     */
    static void exportDI4(AppControler controler) {
        MainView mainView = controler.getMainView();
        Polytech polytech = controler.getPolytech();

        mainView.getExportButtonDI4().addActionListener(e -> {
            try {
                setCaracteristicsBeforeExportDI4(controler);

                if (String.valueOf(mainView.getTypeBoxDI4().getSelectedItem()).equals(InitControler.getTextExportList()))
                    Utils.exportListToXLS((ModelTable) mainView.getTableDI4().getModel(), polytech.getFileAnalysis(Degree.Fourth).getStudents(), polytech.getFileAnalysis(Degree.Fourth).getFile(), mainView.getFieldDI4().getText());
                else if (String.valueOf(mainView.getTypeBoxDI4().getSelectedItem()).equals(InitControler.getTextExportNotes()))
                    Utils.exportNotesToXLS((ModelTable) mainView.getTableDI4().getModel(), polytech.getFileAnalysis(Degree.Fourth).getStudents(), polytech.getFileAnalysis(Degree.Fourth).getFile(), mainView.getFieldDI4().getText());
                else if (String.valueOf(mainView.getTypeBoxDI4().getSelectedItem()).contains(InitControler.getTextExportEmargment()))
                    Utils.exportListEmargement((ModelTable) mainView.getTableDI4().getModel(), polytech.getFileAnalysis(Degree.Fourth).getStudents(), polytech.getFileAnalysis(Degree.Fourth).getFile(), mainView.getFieldDI4().getText());

                new Notification(mainView, "Exportation de la liste terminée");
            } catch (IOException e1) {
                new Notification(mainView, e1.getMessage());
            }
        });
    }

    /**
     * Fonction définissant quel type d'exportation se fera pour le panel DI5
     *
     * @param controler Le controleur contenant la vue et la modèle, qui permet de récupérer la comboBox pour le type d'exportation et autres objets utiles
     */
    static void exportDI5(AppControler controler) {
        MainView mainView = controler.getMainView();
        Polytech polytech = controler.getPolytech();

        mainView.getExportButtonDI5().addActionListener(e -> {
            try {
                setCaracteristicsBeforeExportDI5(controler);

                if (String.valueOf(mainView.getTypeBoxDI5().getSelectedItem()).contains(InitControler.getTextExportList()))
                    Utils.exportListToXLS((ModelTable) mainView.getTableDI5().getModel(), polytech.getFileAnalysis(Degree.Fifth).getStudents(), polytech.getFileAnalysis(Degree.Fifth).getFile(), mainView.getFieldDI5().getText());
                else if (String.valueOf(mainView.getTypeBoxDI5().getSelectedItem()).contains(InitControler.getTextExportNotes()))
                    Utils.exportNotesToXLS((ModelTable) mainView.getTableDI5().getModel(), polytech.getFileAnalysis(Degree.Fifth).getStudents(), polytech.getFileAnalysis(Degree.Fifth).getFile(), mainView.getFieldDI5().getText());
                else if (String.valueOf(mainView.getTypeBoxDI5().getSelectedItem()).contains(InitControler.getTextExportEmargment()))
                    Utils.exportListEmargement((ModelTable) mainView.getTableDI5().getModel(), polytech.getFileAnalysis(Degree.Fifth).getStudents(), polytech.getFileAnalysis(Degree.Fifth).getFile(), mainView.getFieldDI5().getText());


                new Notification(mainView, "Exportation de la liste terminée");
            } catch (IOException e1) {
                new Notification(mainView, e1.getMessage());
            }
        });
    }

    /**
     * On définit les étudiants particuliers avant l'exportation pour une liste de DI3
     * @param controler Le controleur
     */
    private static void setCaracteristicsBeforeExportDI3(AppControler controler) {
        MainView mainView = controler.getMainView();
        Polytech polytech = controler.getPolytech();
        Properties config = controler.getConfig();

        for (Student s : polytech.getFileAnalysis(Degree.Third).getStudents()) {
            s.setMobilite(false); //on met a jour le statut de mobilité de l'étudiant (pas de mobilité pour les DI3)

            // On cherche si le paramètre de redoublement a été défini dans les paramètres
            if (mainView.getComboBoxColonneRedoublantDI3().getSelectedItem() != null) {
                /*
                On vérifie si la valeur contenue dans le redoublement de l'étudiant est égale à la valeur que l'utilisateur a défini pour la couleur du redoublement
                */
                if (s.getCaracteristics().get(polytech.getFileAnalysis(Degree.Third).getColumnIndex(String.valueOf(
                        config.getProperty("redoubleColDI3"))) + 3).equals(config.getProperty("redoubleValDI3")))
                    s.setRedoublant(true);
                else
                    s.setRedoublant(false);
            } else
                s.setRedoublant(false);

            // On cherche si le paramètre de mundus a été défini dans les paramètres
            if (mainView.getComboBoxColonneMundus().getSelectedItem() != null) {
                /*
                On vérifie si la valeur contenue dans le mundus de l'étudiant est égale à la valeur que l'utilisateur a défini pour la couleur des mundus
                */
                if (s.getCaracteristics().get(polytech.getFileAnalysis(Degree.Third).getColumnIndex(String.valueOf(
                        config.getProperty("mundusCol"))) + 3).equals(config.getProperty("mundusVal")))
                    s.setMundus(true);
                else
                    s.setMundus(false);
            }
        }
    }

    /**
     * On définit les étudiants particuliers avant une exportation de liste pour DI4
     * @param controler Le controleur
     */
    private static void setCaracteristicsBeforeExportDI4(AppControler controler) {
        MainView mainView = controler.getMainView();
        Polytech polytech = controler.getPolytech();
        Properties config = controler.getConfig();

        for (Student s : polytech.getFileAnalysis(Degree.Fourth).getStudents()) {
            if (mainView.getComboBoxColonneMobiliteDI4().getSelectedItem() != null) {
                /*
                On vérifie si la valeur contenue dans la mobilité de l'étudiant est égale à la valeur que l'utilisateur a défini pour la couleur de la mobilité
                */
                if (s.getCaracteristics().get(polytech.getFileAnalysis(Degree.Fourth).getColumnIndex(String.valueOf(
                        config.getProperty("mobiliteColDI4"))) + 3).equals(config.getProperty("mobiliteValDI4")))
                    s.setMobilite(true);
                else
                    s.setMobilite(false);
            } else {
                s.setMobilite(false);
            }
            if (mainView.getComboBoxColonneRedoublantDI4().getSelectedItem() != null) {
                /*
                On vérifie si la valeur contenue dans le redoublement de l'étudiant est égale à la valeur que l'utilisateur a défini pour la couleur du redoublement
                */
                if (s.getCaracteristics().get(polytech.getFileAnalysis(Degree.Fourth).getColumnIndex(String.valueOf(
                        config.getProperty("redoubleColDI4"))) + 3).equals(config.getProperty("redoubleValDI4")))
                    s.setRedoublant(true);
                else
                    s.setRedoublant(false);
            } else
                s.setRedoublant(false);
        }
    }

    /**
     * On définit les étudiants particuliers avant l'exportation d'une liste pour DI5
     * @param controler Le controleur
     */
    private static void setCaracteristicsBeforeExportDI5(AppControler controler) {
        MainView mainView = controler.getMainView();
        Polytech polytech = controler.getPolytech();
        Properties config = controler.getConfig();

        /*
        On parcourt tous les étudiants du fichier des DI5
         */
        for (Student s : polytech.getFileAnalysis(Degree.Fifth).getStudents()) {
            if (mainView.getComboBoxColonneMobiliteDI5().getSelectedItem() != null) {
                /*
                On vérifie si la valeur contenue dans la mobilité de l'étudiant est égale à la valeur que l'utilisateur a défini pour la couleur de la mobilité
                */
                if (s.getCaracteristics().get(polytech.getFileAnalysis(Degree.Fifth).getColumnIndex(String.valueOf(
                        config.getProperty("mobiliteColDI5"))) + 3).equals(config.getProperty("mobiliteValDI5")))
                    s.setMobilite(true);
                else
                    s.setMobilite(false);
            } else
                s.setMobilite(false);
            if (mainView.getComboBoxColonneRedoublantDI5().getSelectedItem() != null) {
                /*
                On vérifie si la valeur contenue dans le redoublement de l'étudiant est égale à la valeur que l'utilisateur a défini pour la couleur du redoublement
                */
                if (s.getCaracteristics().get(polytech.getFileAnalysis(Degree.Fifth).getColumnIndex(String.valueOf(
                        config.getProperty("redoubleColDI5"))) + 3).equals(config.getProperty("redoubleValDI5")))
                    s.setRedoublant(true);
                else
                    s.setRedoublant(false);
            } else
                s.setRedoublant(false);
        }
    }
}

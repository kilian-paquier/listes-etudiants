package com.model;

import java.util.HashMap;
import java.util.Map;

public class Student {
    private String studentId; // Le numéro d'étudiant de l'étudiant
    private String firstName; // Le prénom de l'étudiant
    private String lastName; // Le nom de l'étudiant
    private Map<Integer,String> caracteristics; //
    private boolean mobilite = false; // Booléen permettant de savoir si l'étudiant part en mobilité sur un semestre
                                    // de l'année
    private boolean redoublant = false; // Booléen permettant de savoir si l'étudiant est un redoublant
    private boolean mundus = false; // Booléen permettant de savoir si l'étudiant est mundus

    /**
     * Constructeur
     * @param firstName Le prénom de l'étudiant
     * @param lastName Le nom de l'étudiant
     * @param studentId Le numéro d'étudiant de l'étudiant
     */
    public Student(String firstName, String lastName, String studentId) {
        caracteristics = new HashMap<>();
        this.firstName = firstName;
        this.lastName = lastName;
        this.studentId = studentId;
    }

    /**
     * Getter de la mobilité
     * @return Retourne le booléen de la mobilité
     */
    boolean isMobilite() {
        return mobilite;
    }

    /**
     * Setter de la mobilité
     * @param statut Le nouveau booléen de la mobilité
     */
    public void setMobilite(boolean statut) {
        this.mobilite = statut;
    }

    /**
     * Getter du booléen de Mundus
     * @return Retourne le booléen de Mundus
     */
    boolean isMundus() {
        return mundus;
    }

    /**
     * Setter du booléen de Mundus
     * @param mundus Le nouveau booléen pour Mundus
     */
    public void setMundus(boolean mundus) {
        this.mundus = mundus;
    }

    /**
     * Getter du booléen pour le redoublement
     * @return Le booléen du redoublement
     */
    boolean isRedoublant() {
        return redoublant;
    }

    /**
     * Setter du booléen pour le redoublement
     * @param redoublant Le nouveau booléen pour le redoublement
     */
    public void setRedoublant(boolean redoublant) {
        this.redoublant = redoublant;
    }

    /**
     * Getter du numéro d'étudiant
     * @return Le numéro d'étudiant
     */
    String getStudentId() {
        return studentId;
    }

    /**
     * Setter du numéro d'étudiant
     * @param studentId Le nouveau numéro d'étudiant
     */
    void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    /**
     * Getter du prénom de l'étudiant
     * @return Le prénom de l'étudiant
     */
    String getFirstName() {
        return firstName;
    }

    /**
     * Setter du prénom de l'étudiant
     * @param firstName Le nouveau prénom
     */
    void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Getter de nom de l'étudiant
     * @return Le nom de l'étudiant
     */
    String getLastName() {
        return lastName;
    }

    /**
     * Setter du nom de l'étudiant
     * @param lastName Le nouveau nom de l'étudiant
     */
    void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Getter des caractéristiques
     * @return Les caractéristiques de l'étudiant
     */
    public Map<Integer, String> getCaracteristics() {
        return caracteristics;
    }
}

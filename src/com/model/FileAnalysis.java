package com.model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileAnalysis {
    private Degree degree; // Le degré du fichier analysé
    private List<Student> students = new ArrayList<>(); // La liste des étudiants du fichier analysé
    private List<Column> columns = new ArrayList<>(); // Les colonnes parsés depuis le fichier
    private File file; // Le fichier
    private List<Template> templates = new ArrayList<>(); // La liste des templates correspondants au degré

    /**
     * Constructeur
     */
    public FileAnalysis() {

    }

    /**
     * Constructeur prenant en paramètre un degré
     * @param degree Le degré du fichier
     */
    public FileAnalysis(Degree degree) {
        this.degree = degree;
    }

    /**
     * Fonction ajoutant un étudiant à la liste des étudiants, s'il n'existe pas déjà
     * @param student L'étudiant à ajouter
     */
    public void addStudent(Student student) {
        if(student.getStudentId() != null && getStudent(student.getStudentId()) == null)
            students.add(student);
    }

    /**
     * Fonction ajoutant un colonne
     * @param column La colonne à ajouter à la liste des colonnes
     */
    public void addColumn(Column column) {
        if (getColumn(column.getColumnNumber()) == null)
            columns.add(column);
    }

    /**
     * Fonction ajoutant un template à la liste des templates, si le template n'existe pas déjà
     * @param template Le template à ajouter
     * @throws IOException Si le template existe déjà
     */
    public void addTemplate(Template template) throws IOException {
        if (templateContained(template))
            throw new IOException("Le modèle du template existe déjà");
        templates.add(template);
    }

    /**
     * Fonction retournant la liste des templates
     * @return La liste des templates
     */
    public List<Template> getTemplates() {
        return templates;
    }

    /**
     * Fonction retournant le degré du fileAnalisys
     * @return Le degré retourné
     */
    public Degree getDegree() {
        return degree;
    }

    /**
     * Setter du degré
     * @param degree Le nouveau degré
     */
    public void setDegree(Degree degree) {
        this.degree = degree;
    }

    /**
     * Getter de la liste des étudiants
     * @return La liste des étudiants
     */
    public List<Student> getStudents() {
        return students;
    }

    /**
     * Getter de la liste des colonnes
     * @return La liste des colonnes
     */
    public List<Column> getColumns() {
        return columns;
    }

    /**
     * Fonction retournant une colonne selon le numéro de colonne passé en paramètre
     * @param columnIndex Le numéro de colonne
     * @return La colonne retournée si elle existe
     */
    public Column getColumn(int columnIndex) {
        for (Column column : columns) {
            if (column.getColumnNumber() == columnIndex)
                return column;
        }
        return null;
    }

    /**
     * Fonction retournant une colonne en fonction du nom de la colonne
     * @param columnName Le nom de la colonne voulue
     * @return La colonne si elle existe
     */
    public Column getColumn(String columnName) {
        for (Column column : columns) {
            if (column.getColumnName().equals(columnName)) {
                return column;
            }
        }
        return null;
    }

    public int getColumnIndex(String columnName) {
        for (int index = 0; index < columns.size(); index++) {
            if (columns.get(index).equals(getColumn(columnName))) {
                return index;
            }
        }
        throw new RuntimeException("la colonne " + columnName + " n'existe pas !");
    }

    /**
     * Getter du fichier
     * @return Le fichier
     */
    public File getFile() {
        return file;
    }

    /**
     * Setter du fichier
     * @param file Le nouveau fichier
     */
    public void setFile(File file) {
        this.file = file;
    }

    /**
     * Fonction supprimant un template dans la liste en fonction du nom passé en paramètre
     * @param templateName Le nom du template à supprimer
     */
    public void deleteTemplate(String templateName) {
        for (Template template : templates) {
            if (template.getTemplateName().equals(templateName)) {
                templates.remove(template);
                break;
            }
        }
    }

    /**
     * Fonction retournant un template en fonction de son nom
     * @param templateName Le nom du template voulu
     * @return Le template s'il existe
     */
    public Template getTemplate(String templateName) {
        for (Template temp : templates) {
            if (temp.getTemplateName().equals(templateName))
                return  temp;
        }
        return null;
    }

    /**
     * Fonction retournant un booléen disant si le template passé en paramètre existe déjà dans la liste
     * La comparaison se fait sur le modèle en comparant les deux vecteurs de vecteurs
     * @param template Le template passé en paramètre
     * @return Le booléen, vrai si le template existe dans la liste
     */
    private boolean templateContained(Template template) {
        for (Template templateThis : templates) {
            if (templateThis.compareTo(template) == 0 && templateThis.getTemplateName().equals(template.getTemplateName()))
                return true;
        }
        return false;
    }

    /**
     * Fonction retournant un étudiant selon son numéro d'étudiant
     * @param studentID Le numéro d'étudiant
     * @return L'étudiant s'il existe
     */
    public Student getStudent(String studentID) {
        for (Student student : students) {
            if (student.getStudentId().equals(studentID))
                return student;
        }
        return null;
    }
}

package com.controler;

import com.view.MainView;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

abstract class ActionTableControler {
    /**
     * Fonction définissant les actions pour le tableau des DI3
     *
     * @param controler Le controler contenant la vue et le modèle
     */
    static void actionTableDI3(AppControler controler) {
        MainView mainView = controler.getMainView();

        mainView.getTableDI3().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                multiSelection(mainView.getTableDI3()); // MouseListener permettant la sélection des cases du tableau
            }
        });

        final long[] eWhen1 = {0}, eWhen2 = {0};
        mainView.getTableDI3().getTableHeader().addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mouseClicked(e);
                eWhen1[0] = e.getWhen();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                eWhen2[0] = e.getWhen();
                if (eWhen2[0] - eWhen1[0] <= 150) {
                    int col = mainView.getTableDI3().getTableHeader().columnAtPoint(e.getPoint());
                    columnSelection(mainView.getTableDI3(), col); // MouseListener permettant la sélection de tout une colonne
                }
            }
        });
    }

    /**
     * Fonction définissant les actions du tableau DI4
     *
     * @param controler Le controleur contenant la vue et la modèle
     */
    static void actionTableDI4(AppControler controler) {
        MainView mainView = controler.getMainView();

        mainView.getTableDI4().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                multiSelection(mainView.getTableDI4()); // MouseListener pour la sélection des cases
            }
        });

        final long[] eWhen1 = {0}, eWhen2 = {0};
        mainView.getTableDI4().getTableHeader().addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mouseClicked(e);
                eWhen1[0] = e.getWhen();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                eWhen2[0] = e.getWhen();
                if (eWhen2[0] - eWhen1[0] <= 150) {
                    int col = mainView.getTableDI4().getTableHeader().columnAtPoint(e.getPoint());
                    columnSelection(mainView.getTableDI4(), col); // MouseListener permettant la sélection de tout une colonne
                }
            }
        });
    }

    /**
     * Fonction définissant les actions du tableau des DI5
     *
     * @param controler Le controleur contenant la vue et le modèle
     */
    static void actionTableDI5(AppControler controler) {
        MainView mainView = controler.getMainView();

        mainView.getTableDI5().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                multiSelection(mainView.getTableDI5()); // Action de sélection des cases
            }
        });

        final long[] eWhen1 = {0}, eWhen2 = {0};
        mainView.getTableDI5().getTableHeader().addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mouseClicked(e);
                eWhen1[0] = e.getWhen();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                eWhen2[0] = e.getWhen();
                if (eWhen2[0] - eWhen1[0] <= 150) {
                    int col = mainView.getTableDI5().getTableHeader().columnAtPoint(e.getPoint());
                    columnSelection(mainView.getTableDI5(), col); // MouseListener permettant la sélection de tout une colonne
                }
            }
        });
    }

    /**
     * Fonction de multisélection d'un table
     *
     * @param table La table à sélectionner
     */
    private static void multiSelection(JTable table) {
        int[] colNums = table.getSelectedColumns();
        int[] rowNums = table.getSelectedRows();

        // Pour plusieurs colonnes
        List<Integer> colnums = new ArrayList<>();
        for (int num : colNums) colnums.add(num);

        for (int colNum : colNums) {
            // Multi sélection de plusieurs colonnes (0 va avec 1, 2 avec 3 etc ...)
            if (colnums.contains(colNum - 1) && colNum % 2 == 1)
                continue;
            for (int rowNum : rowNums) {
                selection(table, colNum, rowNum);
            }
        }
    }

    /**
     * Fonction réalisant la sélection sur une colonne entière
     *
     * @param table La table
     * @param col   La colonne sélectionnée
     */
    private static void columnSelection(JTable table, int col) {
        if (col != -1) {
            table.clearSelection();
            table.setColumnSelectionInterval(col, col);
            allSelection(table, table.getRowCount());
        }
    }

    /**
     * Fonction pour la séléction de toutes case contenant des informations dans une colonne
     *
     * @param table La table
     * @param rows  Le nombre de lignes de la table
     */
    private static void allSelection(JTable table, int rows) {
        int colNum = table.getSelectedColumn();
        boolean bFalse = false; // Booleen : true si une case au moins de la colonne est séléctionnée
        boolean bTrue = false; // Booleen : true si au moins une case de la colonne n'est pas séléctionnée

        //on parcourt toutes les lignes de la JTable
        for (int rowNum = 0; rowNum < rows; rowNum++) {
            if (colNum % 2 == 0) { // Colonne paire (contient les valeurs)
                if (table.getValueAt(rowNum, colNum) != null) { // On verifie que la cellule contient une valeur
                    if (table.getValueAt(rowNum, colNum + 1) == Boolean.FALSE)
                        bTrue = true;
                    else
                        bFalse = true;
                } else {
                    if (table.getValueAt(rowNum, colNum) == Boolean.FALSE)
                        bTrue = true;
                    else
                        bFalse = true;
                }
            } else { // Colonne impaire (contient les case à cocher)
                if (table.getValueAt(rowNum, colNum) == Boolean.FALSE)
                    bTrue = true;
                else
                    bFalse = true;
            }
        }
        // Si la colonne ne contient que des cases non cochées ou cochées et non utiles alors on coche ou décoche les cases utiles
        if ((bFalse && !bTrue) || (bTrue && !bFalse)) {
            for (int rowNum = 0; rowNum < rows; rowNum++)
                selection(table, colNum, rowNum);
        }
        // Si la colonne contient des cases cochées et non cochées, on coche toutes les cases
        else {
            for (int rowNum = 0; rowNum < rows; rowNum++) {
                if (colNum % 2 == 0) {
                    if (table.getValueAt(rowNum, colNum) != null && table.getValueAt(rowNum, colNum + 1) == Boolean.FALSE)
                        table.setValueAt(Boolean.TRUE, rowNum, colNum + 1);
                } else {
                    if (table.getValueAt(rowNum, colNum) != null && table.getValueAt(rowNum, colNum) == Boolean.FALSE)
                        table.setValueAt(Boolean.TRUE, rowNum, colNum);
                }
            }
        }
    }

    /**
     * Fonction réalisant la sélection ou déselection d'une case dans un tableau
     *
     * @param table  le tableau
     * @param colNum La colonne de la case
     * @param rowNum La ligne de la case
     */
    private static void selection(JTable table, int colNum, int rowNum) {
        /*
        On regarde si on travaille sur la colonne des noms des valeurs
         */
        if (colNum % 2 == 0) {
            // Si la case n'est pas cochée
            if (table.getValueAt(rowNum, colNum) != null && table.getValueAt(rowNum, colNum + 1) == Boolean.FALSE) {
                table.setValueAt(Boolean.TRUE, rowNum, colNum + 1);
            }
            // Si la case est cochée
            else if (table.getValueAt(rowNum, colNum) != null && table.getValueAt(rowNum, colNum + 1) == Boolean.TRUE) {
                table.setValueAt(Boolean.FALSE, rowNum, colNum + 1);
            }
        }
        // On regarde si on travaille sur la colonne des cases des valeurs
        else {
            // Si la case n'est pas cochée
            if (table.getValueAt(rowNum, colNum) == Boolean.FALSE && table.getValueAt(rowNum, colNum) != null) {
                table.setValueAt(Boolean.TRUE, rowNum, colNum);
            }
            // Si la case est cochée
            else if (table.getValueAt(rowNum, colNum) == Boolean.TRUE && table.getValueAt(rowNum, colNum) != null) {
                table.setValueAt(Boolean.FALSE, rowNum, colNum);
            }
        }
    }
}

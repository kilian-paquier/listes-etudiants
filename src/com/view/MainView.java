package com.view;

import javax.swing.*;

public class MainView extends JFrame {

    private JPanel globalPanel;
    private JTable tableDI3;
    private JTable tableDI4;
    private JTable tableDI5;
    private JList<String> listTemplateDI4;
    private JButton ajouterTemplateDI4;
    private JButton supprimerTemplateDI4;
    private JButton modifierTemplateDI4;
    private JList<String> listTemplateDI3;
    private JButton supprimerTemplateDI3;
    private JButton modifierTemplateDI3;
    private JButton ajouterTemplateDI3;
    private JList<String> listTemplateDI5;
    private JButton supprimerTemplateDI5;
    private JButton modifierTemplateDI5;
    private JButton ajouterTemplateDI5;
    private JTabbedPane tabbedPane;
    private JTextField redoublantColorField;
    private JButton redoublantColorButton;
    private JTextField mobiliteColorField;
    private JButton mobiliteColorButton;
    private JTextField mundusColorField;
    private JButton mundusColorButton;
    private JComboBox<String> comboBoxValeurRedoublantDI3;
    private JComboBox<String> comboBoxColonneRedoublantDI3;
    private JComboBox<String> comboBoxColonneRedoublantDI4;
    private JComboBox<String> comboBoxValeurRedoublantDI4;
    private JComboBox<String> comboBoxColonneRedoublantDI5;
    private JComboBox<String> comboBoxValeurRedoublantDI5;
    private JComboBox<String> comboBoxColonneMobiliteDI4;
    private JComboBox<String> comboBoxValeurMobiliteDI4;
    private JComboBox<String> comboBoxColonneMobiliteDI5;
    private JComboBox<String> comboBoxValeurMobiliteDI5;
    private JComboBox<String> comboBoxColonneMundus;
    private JComboBox<String> comboBoxValeurMundus;
    private JButton exportButtonDI3;
    private JTextField fieldDI3;
    private JButton exportButtonDI4;
    private JTextField fieldDI5;
    private JButton exportButtonDI5;
    private JTextField fieldDI4;
    private JLabel labelFichierDI3;
    private JButton changerDeFichierDI3;
    private JLabel labelFileDI4;
    private JButton changerDeFichierDI4;
    private JLabel labelFileDI5;
    private JButton changerDeFichierDI5;
    private JComboBox<String> typeBoxDI3;
    private JComboBox<String> typeBoxDI4;
    private JComboBox<String> typeBoxDI5;

    public JTextField getRedoublantColorField() {
        return redoublantColorField;
    }

    public JButton getRedoublantColorButton() {
        return redoublantColorButton;
    }

    public JTextField getMobiliteColorField() {
        return mobiliteColorField;
    }

    public JButton getMobiliteColorButton() {
        return mobiliteColorButton;
    }

    public JTextField getMundusColorField() {
        return mundusColorField;
    }

    public JButton getMundusColorButton() {
        return mundusColorButton;
    }

    public MainView(String title) {
        super(title);
        setContentPane(globalPanel);
    }

    public void open() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        //setSize(1000, 650);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public JTable getTableDI3() {
        return tableDI3;
    }

    public JTable getTableDI4() {
        return tableDI4;
    }

    public JTable getTableDI5() {
        return tableDI5;
    }

    public JButton getModifierTemplateDI4() {
        return modifierTemplateDI4;
    }

    public JButton getAjouterTemplateDI4() {
        return ajouterTemplateDI4;
    }

    public JButton getSupprimerTemplateDI4() {
        return supprimerTemplateDI4;
    }

    public JList<String> getListTemplateDI4() {
        return listTemplateDI4;
    }

    public JList<String> getListTemplateDI3() {
        return listTemplateDI3;
    }

    public JButton getSupprimerTemplateDI3() {
        return supprimerTemplateDI3;
    }

    public JButton getModifierTemplateDI3() {
        return modifierTemplateDI3;
    }

    public JButton getAjouterTemplateDI3() {
        return ajouterTemplateDI3;
    }

    public JList<String> getListTemplateDI5() {
        return listTemplateDI5;
    }

    public JButton getSupprimerTemplateDI5() {
        return supprimerTemplateDI5;
    }

    public JButton getModifierTemplateDI5() {
        return modifierTemplateDI5;
    }

    public JButton getAjouterTemplateDI5() {
        return ajouterTemplateDI5;
    }

    public JTabbedPane getTabbedPane() {
        return tabbedPane;
    }

    public JComboBox<String> getComboBoxValeurRedoublantDI3() {
        return comboBoxValeurRedoublantDI3;
    }

    public JComboBox<String> getComboBoxColonneRedoublantDI3() {
        return comboBoxColonneRedoublantDI3;
    }

    public JComboBox<String> getComboBoxColonneRedoublantDI4() {
        return comboBoxColonneRedoublantDI4;
    }

    public JComboBox<String> getComboBoxValeurRedoublantDI4() {
        return comboBoxValeurRedoublantDI4;
    }

    public JComboBox<String> getComboBoxColonneRedoublantDI5() {
        return comboBoxColonneRedoublantDI5;
    }

    public JComboBox<String> getComboBoxValeurRedoublantDI5() {
        return comboBoxValeurRedoublantDI5;
    }

    public JComboBox<String> getComboBoxColonneMobiliteDI4() {
        return comboBoxColonneMobiliteDI4;
    }

    public JComboBox<String> getComboBoxValeurMobiliteDI4() {
        return comboBoxValeurMobiliteDI4;
    }

    public JComboBox<String> getComboBoxColonneMobiliteDI5() {
        return comboBoxColonneMobiliteDI5;
    }

    public JComboBox<String> getComboBoxValeurMobiliteDI5() {
        return comboBoxValeurMobiliteDI5;
    }

    public JComboBox<String> getComboBoxColonneMundus() {
        return comboBoxColonneMundus;
    }

    public JComboBox<String> getComboBoxValeurMundus() {
        return comboBoxValeurMundus;
    }

    public JButton getExportButtonDI3() {
        return exportButtonDI3;
    }

    public JTextField getFieldDI3() {
        return fieldDI3;
    }

    public JButton getExportButtonDI4() {
        return exportButtonDI4;
    }

    public JTextField getFieldDI5() {
        return fieldDI5;
    }

    public JButton getExportButtonDI5() {
        return exportButtonDI5;
    }

    public JTextField getFieldDI4() {
        return fieldDI4;
    }

    public JLabel getLabelFichierDI3() {
        return labelFichierDI3;
    }

    public JLabel getLabelFileDI4() {
        return labelFileDI4;
    }

    public JLabel getLabelFileDI5() {
        return labelFileDI5;
    }

    public JButton getChangerDeFichierDI3() {
        return changerDeFichierDI3;
    }

    public JButton getChangerDeFichierDI4() {
        return changerDeFichierDI4;
    }

    public JButton getChangerDeFichierDI5() {
        return changerDeFichierDI5;
    }

    public JComboBox<String> getTypeBoxDI3() {
        return typeBoxDI3;
    }

    public JComboBox<String> getTypeBoxDI4() {
        return typeBoxDI4;
    }

    public JComboBox<String> getTypeBoxDI5() {
        return typeBoxDI5;
    }
}

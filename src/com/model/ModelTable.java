package com.model;

import javax.swing.table.DefaultTableModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class ModelTable extends DefaultTableModel implements Serializable {

    private static final long serialVersionUID = 1L; // Version UID pour la sérialisation
    private Degree degree; // Le degré correspondant du modèle

    /**
     * Fonction retournant le degré du modèle
     * @return Le degré
     */
    public Degree getDegree() {
        return degree;
    }

    /**
     * Fonction changeant le degré du modèle
     * @param degree Le nouveau degré du modèle
     */
    public void setDegree(Degree degree) {
        this.degree = degree;
    }

    /**
     * Fonction retournant le vecteur de vecteur contenant les informations du modèle
     * Le retour est sous forme d'un vecteur (liste) contenant un vecteur (pour chaque ligne)
     * @return Le vecteur de vecteur
     */
    @Override
    public Vector<Vector> getDataVector() {
        Vector<Vector> dataVector = new Vector<>(); // Le vecteur qui sera retourné
        Vector<String> ligne = new Vector<>(); // La ligne actuelle

        for (int rowNum = 0; rowNum < getRowCount(); rowNum++) { // On parcours toutes les lignes du modèle
            for (int colNum = 0; colNum < getColumnCount(); colNum++) { // On parcours toutes les colonnes
                if (getValueAt(rowNum,colNum) == null) // Si la valeur de la cellule est nulle on ajoute null au vecteur
                    ligne.addElement(null);
                else
                    ligne.addElement(getValueAt(rowNum,colNum).toString()); // Sinon on ajoute la valeur.toString()
            }
            dataVector.addElement(new Vector<>(ligne)); // On fait une copie de la ligne dans le vecteur
            ligne.clear(); // On remet la ligne à 0 pour la boucle suivante
        }
        return dataVector;
    }


    /**
     * Fonction changeant l'intégralité du vecteur de vecteur contenant les données du modèles
     * @param dataVector Le nouveau vecteur de vecteur avec les nouvelles données
     * @param columnNames La liste des noms de colonnes
     */
    public void setDataVector(Vector<Vector> dataVector, List<String> columnNames) {
        Vector<String> ligne = new Vector<>(); // La ligne courante
        for (Vector dataVectorLigne : dataVector) { // On parcours toutes les lignes du vecteur à ajouter
            for (Object value : dataVectorLigne) { // On parcours chaque objet (cellule) du vecteur
                if (value == null)
                    ligne.addElement(null); // Si la valeur est nulle on met null à la ligne
                else
                    ligne.addElement(value.toString()); // Sinon on met la valeur.toString()
            }
            this.dataVector.addElement(new Vector<>(ligne)); // On fait une copie de la ligne dans le vecteur du modèle
            ligne.clear(); // On remet à 0 la ligne pour la boucle suivante
        }

        /*
        On ajoute les colonnes au modèle
         */
        this.getColumnNames().clear();
        this.getColumnNames().addAll(columnNames);

        /*
        On set le nombre de ligne et le nombre de colonnes
         */
        setColumnCount(columnNames.size());
        setRowCount(dataVector.size());

    }

    /**
     * Fonction retournant une liste avec les noms de colonnes
     * @return La liste des noms de colonnes
     */
    public List<String> getColumnNames() {
        List<String> columnNames = new ArrayList<>();
        for (int colNum = 0; colNum < getColumnCount(); colNum++) {
            columnNames.add(getColumnName(colNum));
        }
        return columnNames;
    }

    /**
     * Fonction définissant si une cellule du modèle est éditable
     * @param row Le numéro de ligne de la cellule
     * @param column Le numéro de colonne de la cellule
     * @return Un booléen définissant si la cellule est éditable
     */
    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    /**
     * Fonction retournant le classe de la colonne
     * @param columnIndex Le numéro de colonne dont on veut connaître la classe
     * @return La classe de la colonne
     */
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if(columnIndex%2 == 0)
            return super.getColumnClass(columnIndex);
        else
            return Boolean.class;
    }
}

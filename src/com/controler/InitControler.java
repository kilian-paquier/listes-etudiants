package com.controler;

import com.model.*;
import com.view.MainView;
import com.view.Notification;

import java.awt.*;
import java.io.IOException;
import java.util.Properties;

abstract class InitControler {
    private static String textExportNotes = "Liste pour les notes";
    private static String textExportList = "Liste N° Carte - Nom - Prénom";
    private static String textExportEmargment = "Liste d'émargement";

    /**
     * Méthode initialisant le panel DI3
     *
     * @param controler Le controleur contenant la vue, le modèle et la configuration
     */
    static void initPanelDI3(AppControler controler) {
        Polytech polytech = controler.getPolytech();
        MainView mainView = controler.getMainView();
        Properties config = controler.getConfig();

        /*
        Initialisation DI3
         */
        if (polytech.getFileAnalysis(Degree.Third) != null) {
            /*
            On créé le template vide afin de remettre rapidement le tableau en tout décoché
            */
            Template blankTemplate = new Template("Template vide", new ModelTable());
            blankTemplate.getModel().setDataVector(((ModelTable) mainView.getTableDI3().getModel()).getDataVector(), ((ModelTable) mainView.getTableDI3().getModel()).getColumnNames());
            blankTemplate.setTemplateDegree(Degree.Third);
            try {
                polytech.getFileAnalysis(Degree.Third).addTemplate(blankTemplate);
            } catch (IOException e) {
                new Notification(mainView, e.getMessage());
            }

            /*
            On définit les paramètres du tableau
            Le fond de la sélection est blanche et non plus bleu
            On récupère le focus autour de la case pour plus de propreté
             */
            mainView.getTableDI3().setSelectionBackground(Color.WHITE);
            mainView.getTableDI3().setSelectionForeground(Color.BLACK);
            mainView.getTableDI3().setFocusable(false);

            /*
            On récupère les paramètres précédemment enregistrés
             */
            boolean confColRedoubleExists = false, confValRedoubleExists = false, confColMundusExists = false, confValMundusExists = false;
            String confColRedouble = config.getProperty("redoubleColDI3");
            String confValRedouble = config.getProperty("redoubleValDI3");
            String confColMundus = config.getProperty("mundusCol");
            String confValMundus = config.getProperty("mundusVal");

            /*
            On initialise les comboBox des paramètres pour les couleurs d'exportation
             */
            for (Column col : polytech.getFileAnalysis(Degree.Third).getColumns()) {
                mainView.getComboBoxColonneRedoublantDI3().addItem(col.getColumnName());
                mainView.getComboBoxColonneMundus().addItem(col.getColumnName());
                if (col.getColumnName().equals(confColRedouble))
                    confColRedoubleExists = true;
                if (col.getColumnName().equals(confColMundus))
                    confColMundusExists = true;
            }

            // si le nom de colonne associée au redoublement DI3 n'est pas vide ET que la colonne existe
            if (!confColRedouble.equals(" ") && confColRedoubleExists) {
                mainView.getComboBoxColonneRedoublantDI3().setSelectedItem(confColRedouble);
            }

            // si le nom de la colonne associée au mundus n'est pas vide ET que la colonne existe
            if (!confColMundus.equals(" ") && confColMundusExists) {
                mainView.getComboBoxColonneMundus().setSelectedItem(confColMundus);
            }

            // On récupère les valeurs contenues dans la colonne associée au redoublement DI3
            for (String val : polytech.getFileAnalysis(Degree.Third).getColumn(String.valueOf(mainView.getComboBoxColonneRedoublantDI3().getSelectedItem())).getValues()) {
                mainView.getComboBoxValeurRedoublantDI3().addItem(val);
                if (val.equals(confValRedouble))
                    confValRedoubleExists = true;
            }

            // si la valeur associée au redoublement n'est pas vide ET qu'elle existe dans la colonne
            if (!confValRedouble.equals(" ") && confValRedoubleExists) {
                mainView.getComboBoxValeurRedoublantDI3().setSelectedItem(confValRedouble);
            }

            // On récupère les valeurs contenues dans la colonne associée aux mundus
            for (String val : polytech.getFileAnalysis(Degree.Third).getColumn(String.valueOf(mainView.getComboBoxColonneMundus().getSelectedItem())).getValues()) {
                mainView.getComboBoxValeurMundus().addItem(val);
                if (val.equals(confValMundus))
                    confValMundusExists = true;
            }

            // si la valeur associée aux mundus n'est pas vide ET qu'elle existe dans la colonne
            if (!confValMundus.equals(" ") && confValMundusExists) {
                mainView.getComboBoxValeurMundus().setSelectedItem(confValMundus);
            }

            TemplateControler.deserializeTemplates(Degree.Third, controler);
            initTypeBoxes(Degree.Third, controler);
            mainView.getLabelFichierDI3().setText(polytech.getFileAnalysis(Degree.Third).getFile().toString());
        }
    }

    /**
     * Méthode initialisant le panel DI4
     * @param controler Le controleur contenant la vue, le modèle et la configuration
     */
    static void initPanelDI4(AppControler controler) {
        Polytech polytech = controler.getPolytech();
        MainView mainView = controler.getMainView();
        Properties config = controler.getConfig();

        /*
        Initialisation DI4
         */
        if (polytech.getFileAnalysis(Degree.Fourth) != null) {

            //On créé le template vide afin de remettre rapidement le tableau en tout décoché
            Template blankTemplate = new Template("Template vide", new ModelTable());
            blankTemplate.getModel().setDataVector(((ModelTable) mainView.getTableDI4().getModel()).getDataVector(), ((ModelTable) mainView.getTableDI4().getModel()).getColumnNames());
            blankTemplate.setTemplateDegree(Degree.Fourth);
            try {
                polytech.getFileAnalysis(Degree.Fourth).addTemplate(blankTemplate);
            } catch (IOException e) {
                new Notification(mainView, e.getMessage());
            }

            /*
            On définit les paramètres du tableau
            Le fond de la sélection est blanche et non plus bleu
            On récupère le focus autour de la case pour plus de propreté
             */
            mainView.getTableDI4().setSelectionBackground(Color.WHITE);
            mainView.getTableDI4().setSelectionForeground(Color.BLACK);
            mainView.getTableDI4().setFocusable(false);

            /*
            Chargement des paramètres
             */
            boolean confColRedoubleExists = false, confValRedoubleExists = false, confColMobiliteExists = false, confValMobiliteExists = false;
            String confColRedouble = config.getProperty("redoubleColDI4");
            String confValRedouble = config.getProperty("redoubleValDI4");
            String confColMobilite = config.getProperty("mobiliteColDI4");
            String confValMobilite = config.getProperty("mobiliteValDI4");

            /*
            On initialise les comboBox de paramètres
             */
            for (Column col : polytech.getFileAnalysis(Degree.Fourth).getColumns()) {
                mainView.getComboBoxColonneRedoublantDI4().addItem(col.getColumnName());
                mainView.getComboBoxColonneMobiliteDI4().addItem(col.getColumnName());
                if (col.getColumnName().equals(confColRedouble))
                    confColRedoubleExists = true;
                if (col.getColumnName().equals(confColMobilite))
                    confColMobiliteExists = true;
            }

            // si le nom de colonne associée au redoublement DI4 n'est pas vide ET que la colonne existe
            if (!confColRedouble.equals(" ") && confColRedoubleExists) {
                mainView.getComboBoxColonneRedoublantDI4().setSelectedItem(confColRedouble);
            }

            // si le nom de colonne associée a la mibilité DI4 n'est pas vide ET que la colonne existe
            if (!confColMobilite.equals(" ") && confColMobiliteExists) {
                mainView.getComboBoxColonneMobiliteDI4().setSelectedItem(confColMobilite);
            }

            // On récupère les valeurs contenues dans la colonne associée au redoublement DI4
            for (String val : polytech.getFileAnalysis(Degree.Fourth).getColumn(String.valueOf(mainView.getComboBoxColonneRedoublantDI4().getSelectedItem())).getValues()) {
                mainView.getComboBoxValeurRedoublantDI4().addItem(val);
                if (val.equals(confValRedouble))
                    confValRedoubleExists = true;
            }

            // si la valeur associée au redoublement n'est pas vide ET qu'elle existe dans la colonne
            if (!confValRedouble.equals(" ") && confValRedoubleExists) {
                mainView.getComboBoxValeurRedoublantDI4().setSelectedItem(confValRedouble);
            }

            // On récupère les valeurs contenues dans la colonne associée a la mobilité DI4
            for (String val : polytech.getFileAnalysis(Degree.Fourth).getColumn(String.valueOf(mainView.getComboBoxColonneMobiliteDI4().getSelectedItem())).getValues()) {
                mainView.getComboBoxValeurMobiliteDI4().addItem(val);
                if (val.equals(confValMobilite))
                    confValMobiliteExists = true;
            }

            // si la valeur associée a la mobilité n'est pas vide ET qu'elle existe dans la colonne
            if (!confValMobilite.equals(" ") && confValMobiliteExists) {
                mainView.getComboBoxValeurMobiliteDI4().setSelectedItem(confValMobilite);
            }

            TemplateControler.deserializeTemplates(Degree.Fourth, controler);
            initTypeBoxes(Degree.Fourth, controler);
            mainView.getLabelFileDI4().setText(polytech.getFileAnalysis(Degree.Fourth).getFile().toString());
        }
    }

    /**
     * Méthode initialisant le panel DI5
     * @param controler Le controleur contenant la vue, le modèle et la configuration
     */
    static void initPanelDI5(AppControler controler) {
        Polytech polytech = controler.getPolytech();
        MainView mainView = controler.getMainView();
        Properties config = controler.getConfig();

        /*
        Initialisation DI5
         */
        if (polytech.getFileAnalysis(Degree.Fifth) != null) {
            //On créé le template vide afin de remettre rapidement le tableau en tout décoché
            Template blankTemplate = new Template("Template vide", new ModelTable());
            blankTemplate.getModel().setDataVector(((ModelTable) mainView.getTableDI5().getModel()).getDataVector(), ((ModelTable) mainView.getTableDI5().getModel()).getColumnNames());
            blankTemplate.setTemplateDegree(Degree.Fifth);
            try {
                polytech.getFileAnalysis(Degree.Fifth).addTemplate(blankTemplate);
            } catch (IOException e) {
                new Notification(mainView, e.getMessage());
            }

            /*
            On définit les paramètres du tableau
            Le fond de la sélection est blanche et non plus bleu
            On récupère le focus autour de la case pour plus de propreté
             */
            mainView.getTableDI5().setSelectionBackground(Color.WHITE);
            mainView.getTableDI5().setSelectionForeground(Color.BLACK);
            mainView.getTableDI5().setFocusable(false);


            /*
            Chargement des paramètres
             */
            boolean confColRedoubleExists = false, confValRedoubleExists = false, confColMobiliteExists = false, confValMobiliteExists = false;
            String confColRedouble = config.getProperty("redoubleColDI5");
            String confValRedouble = config.getProperty("redoubleValDI5");
            String confColMobilite = config.getProperty("mobiliteColDI5");
            String confValMobilite = config.getProperty("mobiliteValDI5");

            /*
            On initialise les comboBox de paramètres
             */
            for (Column col : polytech.getFileAnalysis(Degree.Fifth).getColumns()) {
                mainView.getComboBoxColonneRedoublantDI5().addItem(col.getColumnName());
                mainView.getComboBoxColonneMobiliteDI5().addItem(col.getColumnName());
                if (col.getColumnName().equals(confColRedouble))
                    confColRedoubleExists = true;
                if (col.getColumnName().equals(confColMobilite))
                    confColMobiliteExists = true;
            }

            // si le nom de colonne associée au redoublement DI5 n'est pas vide ET que la colonne existe
            if (!confColRedouble.equals(" ") && confColRedoubleExists) {
                mainView.getComboBoxColonneRedoublantDI5().setSelectedItem(confColRedouble);
            }

            // si le nom de colonne associée a la mobilité DI5 n'est pas vide ET que la colonne existe
            if (!confColMobilite.equals(" ") && confColMobiliteExists) {
                mainView.getComboBoxColonneMobiliteDI5().setSelectedItem(confColMobilite);
            }

            // On récupère les valeurs contenues dans la colonne associée au redoublement DI5
            for (String val : polytech.getFileAnalysis(Degree.Fifth).getColumn(String.valueOf(mainView.getComboBoxColonneRedoublantDI5().getSelectedItem())).getValues()) {
                mainView.getComboBoxValeurRedoublantDI5().addItem(val);
                if (val.equals(confValRedouble))
                    confValRedoubleExists = true;
            }

            // si la valeur associée au redoublement n'est pas vide ET qu'elle existe dans la colonne
            if (!confValRedouble.equals(" ") && confValRedoubleExists) {
                mainView.getComboBoxValeurRedoublantDI5().setSelectedItem(confValRedouble);
            }

            // On récupère les valeurs contenues dans la colonne associée a la mobilité DI5
            for (String val : polytech.getFileAnalysis(Degree.Fifth).getColumn(String.valueOf(mainView.getComboBoxColonneMobiliteDI5().getSelectedItem())).getValues()) {
                mainView.getComboBoxValeurMobiliteDI5().addItem(val);
                if (val.equals(confValMobilite))
                    confValMobiliteExists = true;
            }

            // si la valeur associée a la mobilité n'est pas vide ET qu'elle existe dans la colonne
            if (!confValMobilite.equals(" ") && confValMobiliteExists) {
                mainView.getComboBoxValeurMobiliteDI5().setSelectedItem(confValMobilite);
            }

            TemplateControler.deserializeTemplates(Degree.Fifth, controler);
            initTypeBoxes(Degree.Fifth, controler);
            mainView.getLabelFileDI5().setText(polytech.getFileAnalysis(Degree.Fifth).getFile().toString());
        }
    }

    /**
     * Méthode définissant les paramètres à la fin de l'instance de l'application pour le panel DI3
     * @param controler Le controleur pour récupérer le modèle, la vue, et la configuration
     */
    static void closingDI3(AppControler controler) {
        Polytech polytech = controler.getPolytech();
        Properties config = controler.getConfig();
        MainView mainView = controler.getMainView();

        /*
        Sauvegarde des paramètres DI3
        */
        if (polytech.getFileAnalysis(Degree.Third) != null) {
            config.setProperty("redoubleColDI3", String.valueOf(mainView.getComboBoxColonneRedoublantDI3().getSelectedItem() == null ?
                    " " : mainView.getComboBoxColonneRedoublantDI3().getSelectedItem()));
            config.setProperty("redoubleValDI3", String.valueOf(mainView.getComboBoxValeurRedoublantDI3().getSelectedItem() == null ?
                    " " : mainView.getComboBoxValeurRedoublantDI3().getSelectedItem()));
            config.setProperty("mundusCol", String.valueOf(mainView.getComboBoxColonneMundus().getSelectedItem() == null ?
                    " " : mainView.getComboBoxColonneMundus().getSelectedItem()));
            config.setProperty("mundusVal", String.valueOf(mainView.getComboBoxValeurMundus().getSelectedItem() == null ?
                    " " : mainView.getComboBoxValeurMundus().getSelectedItem()));
        }
    }

    /**
     * Méthode définissant les paramètres à la fin de l'instance de l'application pour le panel DI5
     *
     * @param controler Le controleur pour récupérer le modèle, la vue, et la configuration
     */
    static void closingDI5(AppControler controler) {
        Polytech polytech = controler.getPolytech();
        Properties config = controler.getConfig();
        MainView mainView = controler.getMainView();

        /*
        Sauvegarde des paramètres DI5
        */
        if (polytech.getFileAnalysis(Degree.Fifth) != null) {
            config.setProperty("redoubleColDI5", String.valueOf(mainView.getComboBoxColonneRedoublantDI5().getSelectedItem() == null ?
                    " " : mainView.getComboBoxColonneRedoublantDI5().getSelectedItem()));
            config.setProperty("mobiliteColDI5", String.valueOf(mainView.getComboBoxColonneMobiliteDI5().getSelectedItem() == null ?
                    " " : mainView.getComboBoxColonneMobiliteDI5().getSelectedItem()));
            config.setProperty("redoubleValDI5", String.valueOf(mainView.getComboBoxValeurRedoublantDI5().getSelectedItem() == null ?
                    " " : mainView.getComboBoxValeurRedoublantDI5().getSelectedItem()));
            config.setProperty("mobiliteValDI5", String.valueOf(mainView.getComboBoxValeurMobiliteDI5().getSelectedItem() == null ?
                    " " : mainView.getComboBoxValeurMobiliteDI5().getSelectedItem()));
        }
    }

    /**
     * Méthode définissant les paramètres à la fin de l'instance de l'application pour le panel DI4
     *
     * @param controler Le controleur pour récupérer le modèle, la vue, et la configuration
     */
    static void closingDI4(AppControler controler) {
        Polytech polytech = controler.getPolytech();
        Properties config = controler.getConfig();
        MainView mainView = controler.getMainView();

        /*
        Sauvegarde des paramètres DI4
        */
        if (polytech.getFileAnalysis(Degree.Fourth) != null) {
            config.setProperty("redoubleColDI4", String.valueOf(mainView.getComboBoxColonneRedoublantDI4().getSelectedItem() == null ?
                    " " : mainView.getComboBoxColonneRedoublantDI4().getSelectedItem()));
            config.setProperty("mobiliteColDI4", String.valueOf(mainView.getComboBoxColonneMobiliteDI4().getSelectedItem() == null ?
                    " " : mainView.getComboBoxColonneMobiliteDI4().getSelectedItem()));
            config.setProperty("redoubleValDI4", String.valueOf(mainView.getComboBoxValeurRedoublantDI4().getSelectedItem() == null ?
                    " " : mainView.getComboBoxValeurRedoublantDI4().getSelectedItem()));
            config.setProperty("mobiliteValDI4", String.valueOf(mainView.getComboBoxValeurMobiliteDI4().getSelectedItem() == null ?
                    " " : mainView.getComboBoxValeurMobiliteDI4().getSelectedItem()));
        }
    }

    /**
     * Méthode initialisant les comboBox contenant les types d'exportation, en fonction du degré du panel et du controleur
     * @param degree Le degré (année) du panel
     * @param controler Le controleur, permettant de récupérer la vue
     */
    private static void initTypeBoxes(Degree degree, AppControler controler) {
        MainView mainView = controler.getMainView();

        if (degree.equals(Degree.Third)) {
            mainView.getTypeBoxDI3().removeAllItems();
            mainView.getTypeBoxDI3().addItem(textExportList);
            mainView.getTypeBoxDI3().addItem(textExportEmargment);
            mainView.getTypeBoxDI3().addItem(textExportNotes);
        }

        if (degree.equals(Degree.Fourth)) {
            mainView.getTypeBoxDI4().removeAllItems();
            mainView.getTypeBoxDI4().addItem(textExportList);
            mainView.getTypeBoxDI4().addItem(textExportEmargment);
            mainView.getTypeBoxDI4().addItem(textExportNotes);
        }

        if (degree.equals(Degree.Fifth)) {
            mainView.getTypeBoxDI5().removeAllItems();
            mainView.getTypeBoxDI5().addItem(textExportList);
            mainView.getTypeBoxDI5().addItem(textExportEmargment);
            mainView.getTypeBoxDI5().addItem(textExportNotes);
        }
    }

    static String getTextExportEmargment() {
        return textExportEmargment;
    }

    static String getTextExportList() {
        return textExportList;
    }

    static String getTextExportNotes() {
        return textExportNotes;
    }
}

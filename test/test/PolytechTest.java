package test;

import com.model.Degree;
import com.model.FileAnalysis;
import com.model.Polytech;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PolytechTest {
    private Polytech polytech;

    @Before
    public void setUp() {
        polytech = new Polytech();

        // FileAnalysis
        polytech.addFileAnalysis(new FileAnalysis(Degree.Fourth));
        polytech.addFileAnalysis(new FileAnalysis(Degree.Third));
    }

    @Test
    public void testResearchAnalysis() {
        assertEquals(polytech.getFileAnalysis(Degree.Fourth).getDegree(), Degree.Fourth);
        assertEquals(polytech.getFileAnalysis(Degree.Third).getDegree(), Degree.Third);
        assertNull(polytech.getFileAnalysis(Degree.Fifth));
    }

    @Test
    public void addFileAnalysis() {
        polytech.addFileAnalysis(new FileAnalysis(Degree.Fifth));
        assertEquals(polytech.getFileAnalysis(Degree.Fifth).getDegree(), Degree.Fifth);

        polytech.addFileAnalysis(new FileAnalysis(Degree.Fourth));
        assertEquals(polytech.getFileAnalyses().size(), 3);
    }
}
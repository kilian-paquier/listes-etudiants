package com.model;

public enum Degree {
    Third, // Pour un degré de troisième année
    Fourth, // Pour un degré de quatrième année
    Fifth // Pour un degré de cinquième année
}
